Source: cowdancer
Maintainer: Debian pbuilder maintenance team <team+pbuilder@tracker.debian.org>
Uploaders:
 Jessica Clarke <jrtc27@debian.org>,
 Mattia Rizzolo <mattia@debian.org>,
Section: utils
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 pbuilder <!nocheck>,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/pbuilder-team/cowdancer
Vcs-Git: https://salsa.debian.org/pbuilder-team/cowdancer.git
Rules-Requires-Root: no

Package: cowdancer
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Copy-on-write directory tree utility
 Tries to make copy-on-write semantics upon hard-link copied
 directory trees generated with 'cp -la'
 .
 'cow-shell' command invokes a shell session. Within that session,
 under the directory cow-shell was invoked,
 cowdancer will create a new file when existing i-nodes are opened for
 write.
 Useful for quick scratch workspace and experimentation.
 .
 For a useful Debian tool, try cowbuilder.
Homepage: https://www.netfort.gr.jp/~dancer/software/cowdancer.html

Package: cowbuilder
Architecture: any
Depends:
 cowdancer,
 pbuilder,
 ${misc:Depends},
 ${shlibs:Depends},
Description: pbuilder running on cowdancer
 'cowbuilder' command is a wrapper for pbuilder which allows using
 pbuilder-like interface over cowdancer environment.
 .
 pbuilder is a tool for building and testing Debian package inside a clean
 chroot, and cowbuilder allows chroot to be recreated using
 hard-linked copies with copy-on-write, which makes creation and
 destruction of chroots fast.

Package: qemubuilder
Architecture: linux-any
Depends:
 debootstrap,
 e2fsprogs,
 pbuilder,
 qemu-system | kvm,
 ${misc:Depends},
 ${shlibs:Depends},
Description: pbuilder using QEMU as backend
 pbuilder implementation for QEMU.
 .
 qemubuilder create -- builds QEMU cow base image.
 .
 qemubuilder update -- updates QEMU cow base image.
 .
 qemubuilder build -- builds package inside QEMU cow base image.
 .
 Gives a pbuilder interface for emulated cross-building environments
 using qemu.
 .
 pbuilder is a tool for building and testing Debian package inside a
 clean chroot, and qemubuilder implements similar experience over
 emulated CPUs. This tool allows building package inside emulated
 Debian environment for different CPU architectures supported by qemu.
Homepage: https://wiki.debian.org/qemubuilder
